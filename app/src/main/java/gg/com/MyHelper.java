package gg.com;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class MyHelper {
    Realm realm;
    RealmResults<People> people;

    public MyHelper(Realm realm) {
        this.realm = realm;
    }

    public void selectFromDB(){
        people = realm.where(People.class).findAll();
    }

    public ArrayList<People> justRefresh(){
        ArrayList<People> listitem = new ArrayList<>();
        for (People p: people){
            listitem.add(p);
        }
        return listitem;
    }
}
